# Minecraft World

## ダウンロードのやり方
1. 上記にあるファイル名（ワールドは全てzipファイルで圧縮しています）をクリックするとページが遷移し、そのページの「Download (〇〇 KB)」を選択するとダウンロードが始まる
2. ダウンロードしたzipファイルを任意の場所で解凍する
3. 「.minecraft」フォルダ内の「saves」フォルダにワールドデータのフォルダを置く
4. Minecraftを起動し、追加されていれば成功

※ 「[https://minecraftjapan.miraheze.org/wiki/配布ワールド](https://minecraftjapan.miraheze.org/wiki/%E9%85%8D%E5%B8%83%E3%83%AF%E3%83%BC%E3%83%AB%E3%83%89)」などの情報参考に
